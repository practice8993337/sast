ARG CONTAINER_REGISTRY_BASE_PATH
FROM ${CONTAINER_REGISTRY_BASE_PATH}/tools/amazoncorretto:alpine-jdk17 as builder
ARG JAR_FILE
ARG PROJECT_NAME
ARG GITLAB_USER_EMAIL
COPY target/${JAR_FILE} ${PROJECT_NAME}.jar
RUN java -Djarmode=layertools -jar ${PROJECT_NAME}.jar extract

FROM ${CONTAINER_REGISTRY_BASE_PATH}/tools/amazoncorretto:alpine-jdk17
RUN mkdir ims/
# -D permits to create an user without password...
RUN addgroup -g 10001 voyagerg \
    && adduser -u 10001 -G voyagerg -h /home/voyager -D voyager \
	&& chown -R voyager:voyagerg ims
USER voyager
WORKDIR /ims/
ENTRYPOINT exec java $JAVA_OPTS org.springframework.boot.loader.JarLauncher
COPY --from=builder dependencies/ . 
COPY --from=builder snapshot-dependencies/ .
COPY --from=builder spring-boot-loader/ .
LABEL image.authors=${GITLAB_USER_EMAIL}
COPY --from=builder application/ .
