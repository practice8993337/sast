package com.bt.ms.im.eposbasket.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bt.ms.im.epos.eposservice.entity.AddToEposBasketRequest;
import com.bt.ms.im.epos.eposservice.entity.AddToEposBasketResponse;
import com.bt.ms.im.epos.eposservice.entity.QueryEposBasketStatusRequest;
import com.bt.ms.im.epos.eposservice.entity.QueryEposBasketStatusResponse;
import com.bt.ms.im.eposbasket.config.AppConstants;
import com.bt.ms.im.eposbasket.entity.Attributes;
import com.bt.ms.im.eposbasket.entity.EposBasketRequest;
import com.bt.ms.im.eposbasket.entity.EposBasketResponse;
import com.bt.ms.im.eposbasket.entity.Items;
import com.bt.ms.im.eposbasket.entity.ResponseBean;
import com.bt.ms.im.eposbasket.entity.ResponseBean.ResponseStatus;
import com.bt.ms.im.eposbasket.mapper.AddToBasketMapper;
import com.bt.ms.im.eposbasket.mapper.QueryEposBasketStatusMapper;
import com.bt.ms.im.eposbasket.repository.EposBasketRepository;

@SpringBootTest
class EposBasketServiceImplTest {

	@InjectMocks
	EposBasketServiceImpl service;

	@Mock
	AddToBasketMapper addToBasketMapper;

	@Mock
	QueryEposBasketStatusMapper basketStatusMapper;

	@Mock
	EposBasketRepository repo;

	@Autowired
	AppConstants appConstants;

	@Test
	void addtoeposbasketWhenSuccess() {
		AddToEposBasketRequest beslRequest = new AddToEposBasketRequest();
		ResponseBean<AddToEposBasketResponse> beslResponse = new ResponseBean<AddToEposBasketResponse>();

		EposBasketResponse addToEposBasketresponse = new EposBasketResponse();
		EposBasketRequest request = new EposBasketRequest();

		request.setBasketId("1");
		request.setOperatorId("2");

		List<Items> itemslist = new ArrayList<Items>();
		Items item = new Items();
		item.setSku("12");
		item.setPrice(BigDecimal.valueOf(0.789));
		item.setGroupId("1");
		item.setEncryptedAttribute("123");

		List<Attributes> attributeslist = new ArrayList<Attributes>();
		Attributes attributes = new Attributes();
		attributes.setIndex(1);
		attributes.setValue("value");
		attributes.setName("pavitra");

		attributeslist.add(attributes);
		item.setAttributes(attributeslist);
		itemslist.add(item);

		request.setItems(itemslist);

		Mockito.when(addToBasketMapper.addItemsToBasket(request)).thenReturn(beslRequest);

		beslResponse.setStatus(ResponseStatus.SUCCESS);

		Mockito.when(repo.addItems(beslRequest)).thenReturn(beslResponse);

		Mockito.when(addToBasketMapper.addItemsResponse(beslResponse)).thenReturn(addToEposBasketresponse);

		ResponseBean<EposBasketResponse> res = service.addToEposBasketService(request);

		Assertions.assertAll("res", () -> assertEquals(ResponseStatus.SUCCESS, res.getStatus()));

	}

	@Test
	void addtoeposbasketWhenFailure() {
		AddToEposBasketRequest beslRequest = new AddToEposBasketRequest();
		ResponseBean<AddToEposBasketResponse> beslResponse = new ResponseBean<AddToEposBasketResponse>();

		EposBasketResponse addToEposBasketresponse = new EposBasketResponse();
		EposBasketRequest request = new EposBasketRequest();

		request.setBasketId("1");
		request.setOperatorId("1");

		List<Items> itemslist = new ArrayList<Items>();
		Items item = new Items();
		item.setSku("12");
		item.setPrice(BigDecimal.valueOf(0.789));
		item.setGroupId("1");
		item.setEncryptedAttribute("123");

		List<Attributes> attributeslist = new ArrayList<Attributes>();
		Attributes attributes = new Attributes();
		attributes.setIndex(1);
		attributes.setValue("value");
		attributes.setName("pavitra");

		attributeslist.add(attributes);
		item.setAttributes(attributeslist);
		itemslist.add(item);

		request.setItems(itemslist);

		Mockito.when(addToBasketMapper.addItemsToBasket(request)).thenReturn(beslRequest);

		beslResponse.setStatus(ResponseStatus.FAILURE);

		Mockito.when(repo.addItems(beslRequest)).thenReturn(beslResponse);

		Mockito.when(addToBasketMapper.addItemsResponse(beslResponse)).thenReturn(addToEposBasketresponse);

		ResponseBean<EposBasketResponse> res = service.addToEposBasketService(request);

		Assertions.assertAll("res", () -> assertEquals(ResponseStatus.FAILURE, res.getStatus()));

	}

	@Test
	void queryEposBasketStatusWhenSuccess() {

		QueryEposBasketStatusRequest beslRequest = new QueryEposBasketStatusRequest();
		ResponseBean<QueryEposBasketStatusResponse> beslResponse = new ResponseBean<QueryEposBasketStatusResponse>();

		com.bt.ms.im.eposbasket.entity.QueryEposBasketStatusResponse queryEposBasketStatusresponse = new com.bt.ms.im.eposbasket.entity.QueryEposBasketStatusResponse();

		Mockito.when(basketStatusMapper.requestMapper("1234")).thenReturn(beslRequest);

		beslResponse.setStatus(ResponseStatus.SUCCESS);

		Mockito.when(repo.getQueryEposBasketStatus(beslRequest)).thenReturn(beslResponse);

		Mockito.when(basketStatusMapper.responseMapper(beslResponse.getData()))
				.thenReturn(queryEposBasketStatusresponse);

		ResponseBean<com.bt.ms.im.eposbasket.entity.QueryEposBasketStatusResponse> res = service
				.getQueryEposBasketStatusService("APIGEE-1234", "1234");

		Assertions.assertAll("res", () -> assertEquals(ResponseStatus.SUCCESS, res.getStatus()));

	}

	@Test
	void queryEposBasketStatusWhenFailure() {

		QueryEposBasketStatusRequest beslRequest = new QueryEposBasketStatusRequest();
		ResponseBean<QueryEposBasketStatusResponse> beslResponse = new ResponseBean<QueryEposBasketStatusResponse>();

		com.bt.ms.im.eposbasket.entity.QueryEposBasketStatusResponse queryEposBasketStatusresponse = new com.bt.ms.im.eposbasket.entity.QueryEposBasketStatusResponse();

		Mockito.when(basketStatusMapper.requestMapper("1234")).thenReturn(beslRequest);

		beslResponse.setStatus(ResponseStatus.FAILURE);

		Mockito.when(repo.getQueryEposBasketStatus(beslRequest)).thenReturn(beslResponse);

		Mockito.when(basketStatusMapper.responseMapper(beslResponse.getData()))
				.thenReturn(queryEposBasketStatusresponse);
		ResponseBean<com.bt.ms.im.eposbasket.entity.QueryEposBasketStatusResponse> res = service
				.getQueryEposBasketStatusService("APIGEE-1234", "1234");

		Assertions.assertAll("res", () -> assertEquals(ResponseStatus.FAILURE, res.getStatus()));

	}
}
