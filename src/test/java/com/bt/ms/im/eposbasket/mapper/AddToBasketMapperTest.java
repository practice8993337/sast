package com.bt.ms.im.eposbasket.mapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bt.ms.im.epos.eposservice.entity.AddToEposBasketRequest;
import com.bt.ms.im.epos.eposservice.entity.AddToEposBasketResponse;
import com.bt.ms.im.epos.eposservice.entity.AddToEposBasketResponse.Message;
import com.bt.ms.im.epos.eposservice.entity.BasketStatus;
import com.bt.ms.im.eposbasket.entity.Attributes;
import com.bt.ms.im.eposbasket.entity.EposBasketRequest;
import com.bt.ms.im.eposbasket.entity.EposBasketResponse;
import com.bt.ms.im.eposbasket.entity.Items;
import com.bt.ms.im.eposbasket.entity.ResponseBean;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AddToBasketMapperTest {

	@Autowired
	AddToBasketMapper mapper;

	@Test
	void addItemsToBasketSuccess() {

		EposBasketRequest request = new EposBasketRequest();

		request.setBasketId("1");
		request.setOperatorId("2");

		List<Items> itemslist = new ArrayList<Items>();
		Items item = new Items();
		item.setSku("12");
		item.setPrice(BigDecimal.valueOf(0.789));
		item.setGroupId("1");
		item.setEncryptedAttribute("123");

		List<Attributes> attributeslist = new ArrayList<Attributes>();
		Attributes attributes = new Attributes();
		attributes.setIndex(1);
		attributes.setValue("value");
		attributes.setName("pavitra");

		attributeslist.add(attributes);
		item.setAttributes(attributeslist);
		itemslist.add(item);

		request.setItems(itemslist);

		AddToEposBasketRequest res = mapper.addItemsToBasket(request);
		Assertions.assertEquals("1", res.getMessage().getBasket().getBasketId());

	}

	@Test
	void addItemsResponseSuccess() {

		AddToEposBasketResponse basketResponse = new AddToEposBasketResponse();
		Message message = new Message();
		message.setBasketId("1");
		message.setBasketStatus(BasketStatus.COMPLETE);
		basketResponse.setMessage(message);

		EposBasketResponse res = mapper.addItemsResponse(ResponseBean.of(basketResponse));
		Assertions.assertEquals("1", res.getBasketId());
	}

	@Test
	void addItemsResponseSuccessWhenMessageNull() {

		AddToEposBasketResponse basketResponse = new AddToEposBasketResponse();
		basketResponse.setMessage(null);

		EposBasketResponse res = mapper.addItemsResponse(ResponseBean.of(basketResponse));
		Assertions.assertNotNull(res);
	}

	@Test
	void addItemsResponseSuccessWhenDataNull() {

		AddToEposBasketResponse basketResponse = null;

		EposBasketResponse res = mapper.addItemsResponse(ResponseBean.of(basketResponse));
		Assertions.assertNotNull(res);
	}

	@Test
	void addItemsResponseSuccessWhenResponseNull() {

		EposBasketResponse res = mapper.addItemsResponse(null);
		Assertions.assertNotNull(res);
	}
}
