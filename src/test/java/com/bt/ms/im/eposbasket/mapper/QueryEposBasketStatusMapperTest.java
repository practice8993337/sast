package com.bt.ms.im.eposbasket.mapper;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bt.ms.im.epos.eposservice.entity.ArrayOfVerificationResponses;
import com.bt.ms.im.epos.eposservice.entity.BasketStatus;
import com.bt.ms.im.epos.eposservice.entity.QueryEposBasketStatusRequest;
import com.bt.ms.im.epos.eposservice.entity.QueryEposBasketStatusResponse;
import com.bt.ms.im.epos.eposservice.entity.QueryEposBasketStatusResponse.Message;
import com.bt.ms.im.epos.eposservice.entity.VerificationResponse;
import com.bt.ms.im.epos.eposservice.entity.VerificationStatus;
import com.bt.ms.im.epos.eposservice.entity.VerificationType;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class QueryEposBasketStatusMapperTest {

	@Autowired
	QueryEposBasketStatusMapper mapper;

	@Test
	void requestMapperSuccess() {

		QueryEposBasketStatusRequest res = mapper.requestMapper("1234");
		Assertions.assertEquals("1234", res.getMessage().getBasketId());
	}

	@Test
	void responseMapperSuccess() {

		QueryEposBasketStatusResponse basketStatusResponse = new QueryEposBasketStatusResponse();
		Message message = new Message();
		message.setBasketStatus(BasketStatus.COMPLETE);
		message.setZeroAuthToken("123232deecdcd");
		ArrayOfVerificationResponses arrayOfVerificationResponses = new ArrayOfVerificationResponses();
		List<VerificationResponse> listOfVerificationResponse = arrayOfVerificationResponses.getVerificationResponse();
		VerificationResponse verificationResponse = new VerificationResponse();
		verificationResponse.setCheckStatus(VerificationStatus.MATCHED);
		verificationResponse.setVerification(VerificationType.AVS_POST_CODE);
		listOfVerificationResponse.add(verificationResponse);
		message.setVerificationResponses(arrayOfVerificationResponses);
		basketStatusResponse.setMessage(message);
		com.bt.ms.im.eposbasket.entity.QueryEposBasketStatusResponse res = mapper.responseMapper(basketStatusResponse);
		Assertions.assertEquals("Complete", res.getBasketStatus());
	}
}
