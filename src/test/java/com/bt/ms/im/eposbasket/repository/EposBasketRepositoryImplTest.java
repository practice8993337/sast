package com.bt.ms.im.eposbasket.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.bt.ms.im.epos.eposservice.addtoeposbasket.AddToEposBasketClient;
import com.bt.ms.im.epos.eposservice.entity.AddToEposBasketRequest;
import com.bt.ms.im.epos.eposservice.entity.AddToEposBasketResponse;
import com.bt.ms.im.epos.eposservice.entity.QueryEposBasketStatusRequest;
import com.bt.ms.im.epos.eposservice.entity.QueryEposBasketStatusResponse;
import com.bt.ms.im.epos.eposservice.queryeposbasketstatus.QueryEposBasketStatusClient;
import com.bt.ms.im.eposbasket.entity.ResponseBean;
import com.bt.ms.im.eposbasket.entity.ResponseBean.ResponseStatus;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EposBasketRepositoryImplTest {

	@Autowired
	EposBasketRepository repo;

	@MockBean
	AddToEposBasketClient addToEposBasketClient;

	@MockBean
	QueryEposBasketStatusClient basketStatusClient;

	@Test
	void addToEposBasketRepoSuccess() {

		AddToEposBasketRequest request = new AddToEposBasketRequest();
		AddToEposBasketResponse response = new AddToEposBasketResponse();

		Mockito.when(addToEposBasketClient.addToEposBasket(Mockito.any(AddToEposBasketRequest.class)))
				.thenReturn(response);

		ResponseBean<AddToEposBasketResponse> res = repo.addItems(request);
		assertEquals(ResponseStatus.SUCCESS, res.getStatus());
	}

	@Test
	void queryEposBasketStatusRepoSuccess() {

		QueryEposBasketStatusRequest request = new QueryEposBasketStatusRequest();
		QueryEposBasketStatusResponse response = new QueryEposBasketStatusResponse();

		Mockito.when(basketStatusClient.queryEposBasketStatus(Mockito.any(QueryEposBasketStatusRequest.class)))
				.thenReturn(response);

		ResponseBean<QueryEposBasketStatusResponse> res = repo.getQueryEposBasketStatus(request);
		assertEquals(ResponseStatus.SUCCESS, res.getStatus());
	}

}
