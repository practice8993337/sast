package com.bt.ms.im.eposbasket.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.bt.ms.im.eposbasket.EposBasketApplication;
import com.bt.ms.im.eposbasket.config.AppConstants;
import com.bt.ms.im.eposbasket.entity.Attributes;
import com.bt.ms.im.eposbasket.entity.EposBasketRequest;
import com.bt.ms.im.eposbasket.entity.EposBasketResponse;
import com.bt.ms.im.eposbasket.entity.Items;
import com.bt.ms.im.eposbasket.entity.QueryEposBasketStatusResponse;
import com.bt.ms.im.eposbasket.entity.ResponseBean;
import com.bt.ms.im.eposbasket.entity.ResponseBean.ResponseStatus;
import com.bt.ms.im.eposbasket.entity.RootExceptionDetails;
import com.bt.ms.im.eposbasket.service.EposBasketService;
import com.bt.ms.im.eposbasket.util.RequestValidator;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = { EposBasketApplication.class, EposBasketControllerTest.class })
@AutoConfigureMockMvc
class EposBasketControllerTest {

	@InjectMocks
	EposBasketController controller;

	@MockBean
	EposBasketService service;

	@Autowired
	AppConstants appConstants;

	@Autowired
	RequestValidator validator;

	@Mock
	RequestValidator mockValidator;

	@Autowired
	MockMvc mockMvc;

	String addUri = "/ee/v1/epos-baskets";
	String queryUri = "/ee/v1/epos-baskets/1234/status";

	@Test
	void addToEposBasketServiceSuccess() throws Exception {

		EposBasketRequest eposBasketrequest = eposBasketRequest();

		ObjectMapper mapper = new ObjectMapper();
		String jsonRequest = mapper.writeValueAsString(eposBasketrequest);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(addUri)
				.header("APIGW-Tracking-Header", "4b8a7018-3d66-11e8-b467-0ed5f89f718")
				.contentType(MediaType.APPLICATION_JSON).content(jsonRequest).characterEncoding("utf-8");
		ResponseBean<EposBasketResponse> response = new ResponseBean<EposBasketResponse>();
		response.setStatus(ResponseStatus.SUCCESS);

		Mockito.when(service.addToEposBasketService(Mockito.any(EposBasketRequest.class))).thenReturn(response);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		assertEquals(201, result.getResponse().getStatus());

	}

	@Test
	void invalidTrackingIdFailure() throws Exception {

		EposBasketRequest eposBasketRequest = eposBasketRequest();

		ObjectMapper mapper = new ObjectMapper();
		String jsonRequest = mapper.writeValueAsString(eposBasketRequest);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(addUri)
				.header("APIGW-Tracking-Header", "*************************").contentType(MediaType.APPLICATION_JSON)
				.content(jsonRequest).characterEncoding("utf-8");
		ResponseBean<EposBasketResponse> response = new ResponseBean<EposBasketResponse>();
		response.setStatus(ResponseStatus.SUCCESS);

		MvcResult result = mockMvc.perform(requestBuilder).andExpect(jsonPath("message").value("Invalid header value"))
				.andExpect(jsonPath("code").value("26")).andReturn();

		assertEquals(400, result.getResponse().getStatus());

	}

	@Test
	void invalidBodyFailure() throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		String jsonRequest = mapper.writeValueAsString(null);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(addUri).accept(MediaType.APPLICATION_JSON)
				.header("APIGW-Tracking-Header", "4b8a7018-3d66-11e8-b467-0ed5f89f718")
				.contentType(MediaType.APPLICATION_JSON).content(jsonRequest).characterEncoding("utf-8");

		MvcResult result = mockMvc.perform(requestBuilder).andExpect(jsonPath("message").value("Invalid body"))
				.andExpect(jsonPath("code").value("22")).andReturn();
		assertEquals(400, result.getResponse().getStatus());
	}

	@Test
	void addToEposBasketServerSideExceptionErrorCode05() throws Exception {

		EposBasketRequest eposBasketRequest = eposBasketRequest();

		ObjectMapper mapper = new ObjectMapper();
		String jsonRequest = mapper.writeValueAsString(eposBasketRequest);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(addUri).accept(MediaType.APPLICATION_JSON)
				.header("APIGW-Tracking-Header", "4b8a7018-3d66-11e8-b467-0ed5f89f718")
				.contentType(MediaType.APPLICATION_JSON).content(jsonRequest).characterEncoding("utf-8");

		RootExceptionDetails rExc = new RootExceptionDetails();
		rExc.setReasonCode("05");
		rExc.setReasonText("Error text as received from back-end");
		rExc.setSource("BTSEL");
		List<RootExceptionDetails> rootExceptions = new ArrayList<RootExceptionDetails>();
		rootExceptions.add(rExc);

		ResponseBean<EposBasketResponse> resp = new ResponseBean<EposBasketResponse>();
		resp.setCode("05");
		resp.setMessage("The service is temporarily unavailable");
		resp.setStatus(ResponseStatus.FAILURE);
		resp.setRootException(rootExceptions);

		Mockito.when(service.addToEposBasketService(Mockito.any(EposBasketRequest.class))).thenReturn(resp);

		MvcResult result = mockMvc.perform(requestBuilder)
				.andExpect(jsonPath("rootException[0].reasonCode").value("05")).andExpect(jsonPath("code").value("05"))
				.andExpect(jsonPath("message").value("The service is temporarily unavailable")).andReturn();
		assertEquals(503, result.getResponse().getStatus());
	}

	@Test
	void addToEposBasketServerSideExceptionErrorCode02() throws Exception {

		EposBasketRequest eposBasketRequest = eposBasketRequest();

		ObjectMapper mapper = new ObjectMapper();
		String jsonRequest = mapper.writeValueAsString(eposBasketRequest);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(addUri).accept(MediaType.APPLICATION_JSON)
				.header("APIGW-Tracking-Header", "4b8a7018-3d66-11e8-b467-0ed5f89f718")
				.contentType(MediaType.APPLICATION_JSON).content(jsonRequest).characterEncoding("utf-8");

		RootExceptionDetails rExc = new RootExceptionDetails();
		rExc.setReasonCode("02");
		rExc.setReasonText("Error text as received from back-end");
		rExc.setSource("BTSEL");
		List<RootExceptionDetails> rootExceptions = new ArrayList<RootExceptionDetails>();
		rootExceptions.add(rExc);

		ResponseBean<EposBasketResponse> resp = new ResponseBean<EposBasketResponse>();
		resp.setCode("02");
		resp.setMessage("Internal server error");
		resp.setStatus(ResponseStatus.FAILURE);
		resp.setRootException(rootExceptions);
		Mockito.when(service.addToEposBasketService(Mockito.any(EposBasketRequest.class))).thenReturn(resp);

		MvcResult result = mockMvc.perform(requestBuilder)
				.andExpect(jsonPath("rootException[0].reasonCode").value("02")).andExpect(jsonPath("code").value("02"))
				.andExpect(jsonPath("message").value("Internal server error")).andReturn();
		assertEquals(500, result.getResponse().getStatus());
	}

	@Test
	void addToEposBasketServerSideExceptionErrorCode60() throws Exception {

		EposBasketRequest eposBasketRequest = eposBasketRequest();

		ObjectMapper mapper = new ObjectMapper();

		String jsonRequest = mapper.writeValueAsString(eposBasketRequest);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(addUri).accept(MediaType.APPLICATION_JSON)
				.header("APIGW-Tracking-Header", "4b8a7018-3d66-11e8-b467-0ed5f89f718")
				.contentType(MediaType.APPLICATION_JSON).content(jsonRequest).characterEncoding("utf-8");

		RootExceptionDetails rExc = new RootExceptionDetails();
		rExc.setReasonCode("60");
		rExc.setReasonText("Error text as received from back-end");
		rExc.setSource("BTSEL");
		List<RootExceptionDetails> rootExceptions = new ArrayList<RootExceptionDetails>();
		rootExceptions.add(rExc);

		ResponseBean<EposBasketResponse> resp = new ResponseBean<EposBasketResponse>();
		resp.setCode("60");
		resp.setMessage("Not Found");
		resp.setStatus(ResponseStatus.FAILURE);
		resp.setRootException(rootExceptions);

		Mockito.when(service.addToEposBasketService(Mockito.any(EposBasketRequest.class))).thenReturn(resp);

		MvcResult result = mockMvc.perform(requestBuilder)
				.andExpect(jsonPath("rootException[0].reasonCode").value("60")).andExpect(jsonPath("code").value("60"))
				.andExpect(jsonPath("message").value("Not Found")).andReturn();
		assertEquals(404, result.getResponse().getStatus());

	}

	@Test
	void addToEposBasketServerSideExceptionErrorCode29() throws Exception {

		EposBasketRequest eposBasketRequest = eposBasketRequest();

		ObjectMapper mapper = new ObjectMapper();

		String jsonRequest = mapper.writeValueAsString(eposBasketRequest);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(addUri).accept(MediaType.APPLICATION_JSON)
				.header("APIGW-Tracking-Header", "4b8a7018-3d66-11e8-b467-0ed5f89f718")
				.contentType(MediaType.APPLICATION_JSON).content(jsonRequest).characterEncoding("utf-8");

		RootExceptionDetails rExc = new RootExceptionDetails();
		rExc.setReasonCode("29");
		rExc.setReasonText("Error text as received from back-end");
		rExc.setSource("BTSEL");
		List<RootExceptionDetails> rootExceptions = new ArrayList<RootExceptionDetails>();
		rootExceptions.add(rExc);

		ResponseBean<EposBasketResponse> resp = new ResponseBean<EposBasketResponse>();
		resp.setCode("29");
		resp.setMessage("Bad Request");
		resp.setStatus(ResponseStatus.FAILURE);
		resp.setRootException(rootExceptions);

		Mockito.when(service.addToEposBasketService(Mockito.any(EposBasketRequest.class))).thenReturn(resp);

		MvcResult result = mockMvc.perform(requestBuilder)
				.andExpect(jsonPath("rootException[0].reasonCode").value("29")).andExpect(jsonPath("code").value("29"))
				.andExpect(jsonPath("message").value("Bad Request")).andReturn();
		assertEquals(400, result.getResponse().getStatus());

	}

	@Test
	void addToEposBasketServerSideExceptionInternalServerError() throws Exception {

		EposBasketRequest eposBasketRequest = eposBasketRequest();

		ObjectMapper mapper = new ObjectMapper();

		String jsonRequest = mapper.writeValueAsString(eposBasketRequest);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(addUri).accept(MediaType.APPLICATION_JSON)
				.header("APIGW-Tracking-Header", "4b8a7018-3d66-11e8-b467-0ed5f89f718")
				.contentType(MediaType.APPLICATION_JSON).content(jsonRequest).characterEncoding("utf-8");

		RootExceptionDetails rExc = new RootExceptionDetails();
		rExc.setReasonCode("02");
		rExc.setReasonText("Error text as received from back-end");
		rExc.setSource("BTSEL");
		List<RootExceptionDetails> rootExceptions = new ArrayList<RootExceptionDetails>();
		rootExceptions.add(rExc);

		ResponseBean<EposBasketResponse> resp = new ResponseBean<EposBasketResponse>();
		resp.setCode("00");
		resp.setMessage("Internal server error");
		resp.setStatus(ResponseStatus.FAILURE);
		resp.setRootException(rootExceptions);

		Mockito.when(service.addToEposBasketService(Mockito.any(EposBasketRequest.class))).thenReturn(resp);

		MvcResult result = mockMvc.perform(requestBuilder)
				.andExpect(jsonPath("rootException[0].reasonCode").value("02")).andExpect(jsonPath("code").value("00"))
				.andExpect(jsonPath("message").value("Internal server error")).andReturn();
		assertEquals(500, result.getResponse().getStatus());

	}

	@Test
	void queryEposBasketStatusServiceSuccess() throws Exception {

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(queryUri).header("APIGW-Tracking-Header",
				"4b8a7018-3d66-11e8-b467-0ed5f89f718");
		ResponseBean<QueryEposBasketStatusResponse> response = new ResponseBean<QueryEposBasketStatusResponse>();
		response.setStatus(ResponseStatus.SUCCESS);

		Mockito.when(service.getQueryEposBasketStatusService(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(response);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		assertEquals(200, result.getResponse().getStatus());

	}

	@Test
	void invalidTrackingIdFailureQueryEpos() throws Exception {

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(queryUri).header("APIGW-Tracking-Header",
				"*************************");
		ResponseBean<QueryEposBasketStatusResponse> response = new ResponseBean<QueryEposBasketStatusResponse>();
		response.setStatus(ResponseStatus.SUCCESS);

		MvcResult result = mockMvc.perform(requestBuilder).andExpect(jsonPath("message").value("Invalid header value"))
				.andExpect(jsonPath("code").value("26")).andReturn();

		assertEquals(400, result.getResponse().getStatus());

	}

	@Test
	void queryEposBasketStatusServerSideExceptionErrorCode05() throws Exception {

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(queryUri).accept(MediaType.APPLICATION_JSON)
				.header("APIGW-Tracking-Header", "4b8a7018-3d66-11e8-b467-0ed5f89f718");

		RootExceptionDetails rExc = new RootExceptionDetails();
		rExc.setReasonCode("05");
		rExc.setReasonText("Error text as received from back-end");
		rExc.setSource("BTSEL");
		List<RootExceptionDetails> rootExceptions = new ArrayList<RootExceptionDetails>();
		rootExceptions.add(rExc);

		ResponseBean<QueryEposBasketStatusResponse> resp = new ResponseBean<QueryEposBasketStatusResponse>();
		resp.setCode("05");
		resp.setMessage("The service is temporarily unavailable");
		resp.setStatus(ResponseStatus.FAILURE);
		resp.setRootException(rootExceptions);

		Mockito.when(service.getQueryEposBasketStatusService(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(resp);

		MvcResult result = mockMvc.perform(requestBuilder)
				.andExpect(jsonPath("rootException[0].reasonCode").value("05")).andExpect(jsonPath("code").value("05"))
				.andExpect(jsonPath("message").value("The service is temporarily unavailable")).andReturn();
		assertEquals(503, result.getResponse().getStatus());

		
	}

	@Test
	void queryEposBasketStatusServerSideExceptionErrorCode02() throws Exception {

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(queryUri).accept(MediaType.APPLICATION_JSON)
				.header("APIGW-Tracking-Header", "4b8a7018-3d66-11e8-b467-0ed5f89f718");

		RootExceptionDetails rExc = new RootExceptionDetails();
		rExc.setReasonCode("02");
		rExc.setReasonText("Error text as received from back-end");
		rExc.setSource("BTSEL");
		List<RootExceptionDetails> rootExceptions = new ArrayList<RootExceptionDetails>();
		rootExceptions.add(rExc);

		ResponseBean<QueryEposBasketStatusResponse> resp = new ResponseBean<QueryEposBasketStatusResponse>();
		resp.setCode("02");
		resp.setMessage("Internal server error");
		resp.setStatus(ResponseStatus.FAILURE);
		resp.setRootException(rootExceptions);
		Mockito.when(service.getQueryEposBasketStatusService(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(resp);

		MvcResult result = mockMvc.perform(requestBuilder)
				.andExpect(jsonPath("rootException[0].reasonCode").value("02")).andExpect(jsonPath("code").value("02"))
				.andExpect(jsonPath("message").value("Internal server error")).andReturn();
		assertEquals(500, result.getResponse().getStatus());
	}

	@Test
	void queryEposBasketStatusServerSideExceptionErrorCode60() throws Exception {

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(queryUri).accept(MediaType.APPLICATION_JSON)
				.header("APIGW-Tracking-Header", "4b8a7018-3d66-11e8-b467-0ed5f89f718");

		RootExceptionDetails rExc = new RootExceptionDetails();
		rExc.setReasonCode("60");
		rExc.setReasonText("Error text as received from back-end");
		rExc.setSource("BTSEL");
		List<RootExceptionDetails> rootExceptions = new ArrayList<RootExceptionDetails>();
		rootExceptions.add(rExc);

		ResponseBean<QueryEposBasketStatusResponse> resp = new ResponseBean<QueryEposBasketStatusResponse>();
		resp.setCode("60");
		resp.setMessage("Not Found");
		resp.setStatus(ResponseStatus.FAILURE);
		resp.setRootException(rootExceptions);

		Mockito.when(service.getQueryEposBasketStatusService(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(resp);

		MvcResult result = mockMvc.perform(requestBuilder)
				.andExpect(jsonPath("rootException[0].reasonCode").value("60")).andExpect(jsonPath("code").value("60"))
				.andExpect(jsonPath("message").value("Not Found")).andReturn();
		assertEquals(404, result.getResponse().getStatus());

	}

	@Test
	void queryEposBasketStatusServerSideExceptionErrorCode29() throws Exception {

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(queryUri).accept(MediaType.APPLICATION_JSON)
				.header("APIGW-Tracking-Header", "4b8a7018-3d66-11e8-b467-0ed5f89f718");

		RootExceptionDetails rExc = new RootExceptionDetails();
		rExc.setReasonCode("29");
		rExc.setReasonText("Error text as received from back-end");
		rExc.setSource("BTSEL");
		List<RootExceptionDetails> rootExceptions = new ArrayList<RootExceptionDetails>();
		rootExceptions.add(rExc);

		ResponseBean<QueryEposBasketStatusResponse> resp = new ResponseBean<QueryEposBasketStatusResponse>();
		resp.setCode("29");
		resp.setMessage("Bad Request");
		resp.setStatus(ResponseStatus.FAILURE);
		resp.setRootException(rootExceptions);

		Mockito.when(service.getQueryEposBasketStatusService(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(resp);

		MvcResult result = mockMvc.perform(requestBuilder)
				.andExpect(jsonPath("rootException[0].reasonCode").value("29")).andExpect(jsonPath("code").value("29"))
				.andExpect(jsonPath("message").value("Bad Request")).andReturn();
		assertEquals(400, result.getResponse().getStatus());

	}

	@Test
	void queryEposBasketStatusServerSideExceptionInternalServerError() throws Exception {

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(queryUri).accept(MediaType.APPLICATION_JSON)
				.header("APIGW-Tracking-Header", "4b8a7018-3d66-11e8-b467-0ed5f89f718");

		RootExceptionDetails rExc = new RootExceptionDetails();
		rExc.setReasonCode("02");
		rExc.setReasonText("Error text as received from back-end");
		rExc.setSource("BTSEL");
		List<RootExceptionDetails> rootExceptions = new ArrayList<RootExceptionDetails>();
		rootExceptions.add(rExc);

		ResponseBean<QueryEposBasketStatusResponse> resp = new ResponseBean<QueryEposBasketStatusResponse>();
		resp.setCode("00");
		resp.setMessage("Internal server error");
		resp.setStatus(ResponseStatus.FAILURE);
		resp.setRootException(rootExceptions);

		Mockito.when(service.getQueryEposBasketStatusService(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(resp);

		MvcResult result = mockMvc.perform(requestBuilder)
				.andExpect(jsonPath("rootException[0].reasonCode").value("02")).andExpect(jsonPath("code").value("00"))
				.andExpect(jsonPath("message").value("Internal server error")).andReturn();
		assertEquals(500, result.getResponse().getStatus());

	}

	private EposBasketRequest eposBasketRequest() {

		EposBasketRequest request = new EposBasketRequest();

		request.setBasketId("1");
		request.setOperatorId("1");

		List<Items> itemslist = new ArrayList<Items>();
		Items item = new Items();
		item.setSku("12");
		item.setPrice(BigDecimal.valueOf(09.0));
		item.setGroupId("1");
		item.setEncryptedAttribute("123");

		List<Attributes> attributeslist = new ArrayList<Attributes>();
		Attributes attributes = new Attributes();
		attributes.setIndex(1);
		attributes.setValue("value");
		attributes.setName("pavitra");

		attributeslist.add(attributes);
		item.setAttributes(attributeslist);
		itemslist.add(item);

		request.setItems(itemslist);

		return request;
	}
}
