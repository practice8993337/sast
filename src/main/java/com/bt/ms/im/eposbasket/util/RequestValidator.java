package com.bt.ms.im.eposbasket.util;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bt.ms.im.eposbasket.config.AppConstants;
import com.bt.ms.im.eposbasket.entity.EposBasketRequest;
import com.bt.ms.im.exception.StandardError;
import com.bt.ms.im.exception.handler.standardexception.BadRequestException;

@Component
public class RequestValidator {

	@Autowired
	AppConstants appConstants;

	public void validateQueryStatus(String basketId, String trackingId) {

		if (basketId == null || basketId.isEmpty()) {
			StandardError errormsg = StandardError.ERR400_23;// missing body field

			throw new BadRequestException(errormsg);
		}

		boolean isValidapigwtrackingHeader = Pattern
				.compile(appConstants.getPattern().getApigwtrackingid(), Pattern.CASE_INSENSITIVE).matcher(trackingId)
				.find();

		if (trackingId == null || trackingId.isEmpty()) {
			StandardError errormsg = StandardError.ERR400_25;// missing body field

			throw new BadRequestException(errormsg);
		}

		if (!(isValidapigwtrackingHeader)) {
			StandardError errormsg = StandardError.ERR400_26;
			throw new BadRequestException(errormsg);
		}
	}

	public void validateAddBasket(EposBasketRequest request) {

		

		String operatorId = request.getOperatorId();

		

		if ((operatorId == null || operatorId.isEmpty())){
			StandardError errormsg = StandardError.ERR400_23;// missing body field

			throw new BadRequestException(errormsg);
		}

		String trackingId = request.getApigwtrackingheader();

		if (trackingId == null || trackingId.isEmpty()) {
			StandardError errormsg = StandardError.ERR400_25;

			throw new BadRequestException(errormsg);
		}

		boolean isValidapigwtrackingHeader = Pattern
				.compile(appConstants.getPattern().getApigwtrackingid(), Pattern.CASE_INSENSITIVE).matcher(trackingId)
				.find();

		if (!(isValidapigwtrackingHeader)) {
			StandardError errormsg = StandardError.ERR400_26;
			throw new BadRequestException(errormsg);
		}

	}

}
