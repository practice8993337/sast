package com.bt.ms.im.eposbasket.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VerificationResponse {
	@JsonProperty("verification")
	private String verification;

	@JsonProperty("checkStatus")
	private String checkStatus;
}
