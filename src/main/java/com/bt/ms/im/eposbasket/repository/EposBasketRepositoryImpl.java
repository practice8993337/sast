package com.bt.ms.im.eposbasket.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bt.ms.im.epos.eposservice.addtoeposbasket.AddToEposBasketClient;
import com.bt.ms.im.epos.eposservice.entity.AddToEposBasketRequest;
import com.bt.ms.im.epos.eposservice.entity.AddToEposBasketResponse;
import com.bt.ms.im.epos.eposservice.entity.QueryEposBasketStatusRequest;
import com.bt.ms.im.epos.eposservice.entity.QueryEposBasketStatusResponse;
import com.bt.ms.im.epos.eposservice.queryeposbasketstatus.QueryEposBasketStatusClient;
import com.bt.ms.im.eposbasket.annotation.ClientInfo;
import com.bt.ms.im.eposbasket.entity.ResponseBean;

@Repository
public class EposBasketRepositoryImpl implements EposBasketRepository {

	@Autowired
	QueryEposBasketStatusClient client;

	@Autowired
	AddToEposBasketClient btselclient;

	@ClientInfo(clientSystem = "BI-CD-CS", compTxnName = "queryEposBasketStatus")
	public ResponseBean<QueryEposBasketStatusResponse> getQueryEposBasketStatus(
			QueryEposBasketStatusRequest queryStatusRequest) {

		QueryEposBasketStatusResponse response = client.queryEposBasketStatus(queryStatusRequest);

		return ResponseBean.of(response);
	}

	@ClientInfo(clientSystem = "BI-CD-CS", compTxnName = "addToEposBaket")
	public ResponseBean<AddToEposBasketResponse> addItems(AddToEposBasketRequest addItemsToBasketRequest) {

		AddToEposBasketResponse response = btselclient.addToEposBasket(addItemsToBasketRequest);

		return ResponseBean.of(response);
	}

}
