package com.bt.ms.im.eposbasket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bt.ms.im.epos.eposservice.entity.AddToEposBasketRequest;
import com.bt.ms.im.epos.eposservice.entity.AddToEposBasketResponse;
import com.bt.ms.im.epos.eposservice.entity.QueryEposBasketStatusRequest;
import com.bt.ms.im.eposbasket.entity.EposBasketRequest;
import com.bt.ms.im.eposbasket.entity.EposBasketResponse;
import com.bt.ms.im.eposbasket.entity.QueryEposBasketStatusResponse;
import com.bt.ms.im.eposbasket.entity.ResponseBean;
import com.bt.ms.im.eposbasket.mapper.AddToBasketMapper;
import com.bt.ms.im.eposbasket.mapper.QueryEposBasketStatusMapper;
import com.bt.ms.im.eposbasket.repository.EposBasketRepository;

@Service
public class EposBasketServiceImpl implements EposBasketService {

	@Autowired
	AddToBasketMapper addMapper;

	@Autowired
	QueryEposBasketStatusMapper queryMapper;

	@Autowired
	EposBasketRepository repo;

	@Override
	public ResponseBean<QueryEposBasketStatusResponse> getQueryEposBasketStatusService(String trackingHeader,
			String basketId) {

		QueryEposBasketStatusRequest queryRequest = queryMapper.requestMapper(basketId);
		ResponseBean<com.bt.ms.im.epos.eposservice.entity.QueryEposBasketStatusResponse> queryResponse = repo
				.getQueryEposBasketStatus(queryRequest);

		if (queryResponse.isSuccess()) {
			QueryEposBasketStatusResponse response = queryMapper.responseMapper(queryResponse.getData());
			return ResponseBean.of(response);
		}

		return ResponseBean.errorRes(queryResponse, QueryEposBasketStatusResponse.class);
	}

	@Override
	public ResponseBean<EposBasketResponse> addToEposBasketService(EposBasketRequest request) {
		AddToEposBasketRequest addItemsToBasketRequest = addMapper.addItemsToBasket(request);

		ResponseBean<AddToEposBasketResponse> btselResponse = repo.addItems(addItemsToBasketRequest);

		if (btselResponse.isSuccess()) {
			EposBasketResponse finalSimResponse = addMapper.addItemsResponse(btselResponse);
			return ResponseBean.of(finalSimResponse);
		}

		return ResponseBean.errorRes(btselResponse, EposBasketResponse.class);

	}

}
