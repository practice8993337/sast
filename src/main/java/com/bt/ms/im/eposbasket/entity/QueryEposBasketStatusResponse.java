package com.bt.ms.im.eposbasket.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QueryEposBasketStatusResponse {

	@JsonProperty("basketStatus")
	private String basketStatus;

	@JsonProperty("zeroAuthToken")
	private String zeroAuthToken;

	@JsonProperty("verificationResponse")
	private List<VerificationResponse> verificationResponseList;
}
