package com.bt.ms.im.eposbasket.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Getter;
import lombok.Setter;

/**
 * Application wise constant , will fetch the value from constants.properties
 * file
 *
 */
@Configuration
@PropertySource("classpath:constants.properties")
@PropertySource(value = "file:./config/constants.properties", ignoreResourceNotFound = true)
@ConfigurationProperties(prefix = "app")
@Getter
@Setter
public class AppConstants {

	private Pattern pattern;
	private ErrorRes errorRes;
	private Header header;
	private String strTrue;
	private String strFalse;

	@Getter
	@Setter
	public static class Pattern {
		private String apigwtrackingid;

	}

	@Getter
	@Setter
	public static class ErrorRes {
		private String message;
		private String internalErrorCode;
		private String clientErrorCode;
		private String validationfailedErrorCode;
		private String clientUnavErrorCode;
		private String tempUnavMessage;
		private String clientNotFoundMessage;
		private String clientNotFoundErrorCode;

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getInternalErrorCode() {
			return internalErrorCode;
		}

		public void setInternalErrorCode(String internalErrorCode) {
			this.internalErrorCode = internalErrorCode;
		}

		public String getClientErrorCode() {
			return clientErrorCode;
		}

		public void setClientErrorCode(String clientErrorCode) {
			this.clientErrorCode = clientErrorCode;
		}

		public String getValidationfailedErrorCode() {
			return validationfailedErrorCode;
		}

		public void setValidationfailedErrorCode(String validationfailedErrorCode) {
			this.validationfailedErrorCode = validationfailedErrorCode;
		}

		public String getClientUnavErrorCode() {
			return clientUnavErrorCode;
		}

		public void setClientUnavErrorCode(String clientUnavErrorCode) {
			this.clientUnavErrorCode = clientUnavErrorCode;
		}

		public String getTempUnavMessage() {
			return tempUnavMessage;
		}

		public void setTempUnavMessage(String tempUnavMessage) {
			this.tempUnavMessage = tempUnavMessage;
		}

		public String getClientNotFoundMessage() {
			return clientNotFoundMessage;
		}

		public void setClientNotFoundMessage(String clientNotFoundMessage) {
			this.clientNotFoundMessage = clientNotFoundMessage;
		}

		public String getClientNotFoundErrorCode() {
			return clientNotFoundErrorCode;
		}

		public void setClientNotFoundErrorCode(String clientNotFoundErrorCode) {
			this.clientNotFoundErrorCode = clientNotFoundErrorCode;
		}

	}

	@Getter
	@Setter
	public static class Header {
		private String accept;

		public String getAccept() {
			return accept;
		}

		public void setAccept(String accept) {
			this.accept = accept;
		}

	}

	public Pattern getPattern() {
		return pattern;
	}

	public void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}

	public ErrorRes getErrorRes() {
		return errorRes;
	}

	public void setErrorRes(ErrorRes errorRes) {
		this.errorRes = errorRes;
	}

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public String getStrTrue() {
		return strTrue;
	}

	public void setStrTrue(String strTrue) {
		this.strTrue = strTrue;
	}

	public String getStrFalse() {
		return strFalse;
	}

	public void setStrFalse(String strFalse) {
		this.strFalse = strFalse;
	}

}