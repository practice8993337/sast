package com.bt.ms.im.eposbasket.entity;

import java.math.BigDecimal;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Items {

	private String sku;

	private BigDecimal price;

	private String groupId;

	private List<Attributes> attributes;

	private String encryptedAttribute;

}
