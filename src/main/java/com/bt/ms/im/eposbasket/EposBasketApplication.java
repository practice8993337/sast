package com.bt.ms.im.eposbasket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.bt.ms.im", "org.ms.analytics.client" })
public class EposBasketApplication {

	public static void main(String[] args) {

		SpringApplication.run(EposBasketApplication.class, args);
	}
}
