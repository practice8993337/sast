package com.bt.ms.im.eposbasket.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EposBasketResponse {

	private String basketId;

	private String basketStatus;

}
