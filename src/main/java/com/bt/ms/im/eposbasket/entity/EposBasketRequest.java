package com.bt.ms.im.eposbasket.entity;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EposBasketRequest {

	private String basketId;

	private String operatorId;

	private List<Items> items;

	private String apigwtrackingheader;

}
