package com.bt.ms.im.eposbasket.repository;

import com.bt.ms.im.epos.eposservice.entity.AddToEposBasketRequest;
import com.bt.ms.im.epos.eposservice.entity.AddToEposBasketResponse;
import com.bt.ms.im.epos.eposservice.entity.QueryEposBasketStatusRequest;
import com.bt.ms.im.epos.eposservice.entity.QueryEposBasketStatusResponse;
import com.bt.ms.im.eposbasket.entity.ResponseBean;

public interface EposBasketRepository {

	ResponseBean<QueryEposBasketStatusResponse> getQueryEposBasketStatus(
			QueryEposBasketStatusRequest queryStatusRequest);

	ResponseBean<AddToEposBasketResponse> addItems(AddToEposBasketRequest addItemsToBasketRequest);
}
