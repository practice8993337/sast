package com.bt.ms.im.eposbasket.controller;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bt.ms.im.eposbasket.config.AppConstants;
import com.bt.ms.im.eposbasket.entity.BaseResponse;
import com.bt.ms.im.eposbasket.entity.EposBasketRequest;
import com.bt.ms.im.eposbasket.entity.EposBasketResponse;
import com.bt.ms.im.eposbasket.entity.QueryEposBasketStatusResponse;
import com.bt.ms.im.eposbasket.entity.ResponseBean;
import com.bt.ms.im.eposbasket.service.EposBasketService;
import com.bt.ms.im.eposbasket.util.RequestValidator;

@RestController
@RequestMapping("/ee/v1/epos-baskets")
@EnableAutoConfiguration
public class EposBasketController {

	@Autowired
	AppConstants appConstants;

	@Autowired
	RequestValidator requestValidator;

	@Autowired
	EposBasketService service;

	@GetMapping("/{basketId}/status")
	public ResponseEntity<QueryEposBasketStatusResponse> getQueryEposBasketStatus(
			@RequestHeader(value = "APIGW-Tracking-Header") String trackingHeader,
			@PathVariable(value = "basketId") String basketId) {

		requestValidator.validateQueryStatus(basketId, trackingHeader);
		ResponseBean<QueryEposBasketStatusResponse> response = service.getQueryEposBasketStatusService(trackingHeader,
				basketId);

		if (response.isSuccess()) {
			return new ResponseEntity<>(response.getData(), HttpStatus.OK);
		} else {
			BaseResponse res1 = new BaseResponse();
			res1.setCode(response.getCode());
			res1.setMessage(response.getMessage());
			res1.setRootException(response.getRootExceptions());
			HttpStatus status1;
			if (response.getCode().equals(this.appConstants.getErrorRes().getClientErrorCode())) {
				status1 = HttpStatus.INTERNAL_SERVER_ERROR;
			} else if (response.getCode().equals(this.appConstants.getErrorRes().getClientUnavErrorCode())) {
				status1 = HttpStatus.SERVICE_UNAVAILABLE;
			} else if (response.getCode().equals(this.appConstants.getErrorRes().getValidationfailedErrorCode())) {
				status1 = HttpStatus.BAD_REQUEST;
			} else if (response.getCode().equals(this.appConstants.getErrorRes().getClientNotFoundErrorCode())) {
				status1 = HttpStatus.NOT_FOUND;
			} else {
				status1 = HttpStatus.INTERNAL_SERVER_ERROR;
			}

			return new ResponseEntity(res1, status1);
		}

	}

	@PostMapping
	public ResponseEntity<EposBasketResponse> addtoeposbasket(
			@RequestHeader(value = "APIGW-Tracking-Header") String apigwtrackingHeader,
			@RequestBody @Valid EposBasketRequest request) {

		request.setApigwtrackingheader(apigwtrackingHeader);

		requestValidator.validateAddBasket(request);

		ResponseBean<EposBasketResponse> response = service.addToEposBasketService(request);

		if (response.isSuccess()) {
			return new ResponseEntity<>(response.getData(), HttpStatus.CREATED);

		} else {
			BaseResponse res = new BaseResponse();
			res.setCode(response.getCode());
			res.setMessage(response.getMessage());
			res.setRootException(response.getRootExceptions());
			HttpStatus status;
			if (response.getCode().equals(this.appConstants.getErrorRes().getClientErrorCode())) {
				status = HttpStatus.INTERNAL_SERVER_ERROR;
			} else if (response.getCode().equals(this.appConstants.getErrorRes().getClientUnavErrorCode())) {
				status = HttpStatus.SERVICE_UNAVAILABLE;
			} else if (response.getCode().equals(this.appConstants.getErrorRes().getValidationfailedErrorCode())) {
				status = HttpStatus.BAD_REQUEST;
			} else if (response.getCode().equals(this.appConstants.getErrorRes().getClientNotFoundErrorCode())) {
				status = HttpStatus.NOT_FOUND;
			} else {
				status = HttpStatus.INTERNAL_SERVER_ERROR;
			}

			return new ResponseEntity(res, status);

		}

	}
}
