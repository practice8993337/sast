package com.bt.ms.im.eposbasket.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.bt.ms.im.epos.eposservice.entity.QueryEposBasketStatusRequest;
import com.bt.ms.im.epos.eposservice.entity.QueryEposBasketStatusRequest.Message;
import com.bt.ms.im.epos.eposservice.entity.QueryEposBasketStatusResponse;
import com.bt.ms.im.epos.eposservice.entity.VerificationResponse;

@Component
public class QueryEposBasketStatusMapper {

	public QueryEposBasketStatusRequest requestMapper(String basketId) {
		QueryEposBasketStatusRequest request = new QueryEposBasketStatusRequest();
		Message message = new Message();
		message.setBasketId(basketId);
		request.setMessage(message);

		return request;
	}

	public com.bt.ms.im.eposbasket.entity.QueryEposBasketStatusResponse responseMapper(
			QueryEposBasketStatusResponse response) {

		com.bt.ms.im.eposbasket.entity.QueryEposBasketStatusResponse mapResponse = new com.bt.ms.im.eposbasket.entity.QueryEposBasketStatusResponse();
		mapResponse.setBasketStatus(response.getMessage().getBasketStatus().value());
		mapResponse.setZeroAuthToken(response.getMessage().getZeroAuthToken());

		List<com.bt.ms.im.eposbasket.entity.VerificationResponse> verificationResponses = new ArrayList<>();

		for (VerificationResponse listObject : response.getMessage().getVerificationResponses()
				.getVerificationResponse()) {

			com.bt.ms.im.eposbasket.entity.VerificationResponse verificationResponse = new com.bt.ms.im.eposbasket.entity.VerificationResponse();
			verificationResponse.setVerification(listObject.getVerification().value());
			verificationResponse.setCheckStatus(listObject.getCheckStatus().value());
			verificationResponses.add(verificationResponse);
		}

		mapResponse.setVerificationResponseList(verificationResponses);

		return mapResponse;
	}
}
