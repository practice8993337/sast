package com.bt.ms.im.eposbasket.service;

import com.bt.ms.im.eposbasket.entity.EposBasketRequest;
import com.bt.ms.im.eposbasket.entity.EposBasketResponse;
import com.bt.ms.im.eposbasket.entity.QueryEposBasketStatusResponse;
import com.bt.ms.im.eposbasket.entity.ResponseBean;

public interface EposBasketService {

	ResponseBean<QueryEposBasketStatusResponse> getQueryEposBasketStatusService(String trackingHeader, String basketId);

	ResponseBean<EposBasketResponse> addToEposBasketService(EposBasketRequest request);
}
