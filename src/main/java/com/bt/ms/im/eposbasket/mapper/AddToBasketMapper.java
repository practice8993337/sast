package com.bt.ms.im.eposbasket.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.bt.ms.im.epos.eposservice.entity.AddToEposBasketRequest;
import com.bt.ms.im.epos.eposservice.entity.AddToEposBasketRequest.Message;
import com.bt.ms.im.epos.eposservice.entity.AddToEposBasketResponse;
import com.bt.ms.im.epos.eposservice.entity.ArrayOfBasketItemAttributes;
import com.bt.ms.im.epos.eposservice.entity.ArrayOfBasketItems;
import com.bt.ms.im.epos.eposservice.entity.Basket;
import com.bt.ms.im.epos.eposservice.entity.BasketItem;
import com.bt.ms.im.epos.eposservice.entity.BasketItemAttribute;
import com.bt.ms.im.eposbasket.entity.Attributes;
import com.bt.ms.im.eposbasket.entity.EposBasketRequest;
import com.bt.ms.im.eposbasket.entity.EposBasketResponse;
import com.bt.ms.im.eposbasket.entity.Items;
import com.bt.ms.im.eposbasket.entity.ResponseBean;

@Component
public class AddToBasketMapper {

	public AddToEposBasketRequest addItemsToBasket(EposBasketRequest request) {
		AddToEposBasketRequest btselrequest = new AddToEposBasketRequest();
		Message message = new Message();

		Basket basket = new Basket();

		basket.setBasketId(request.getBasketId());

		basket.setOperatorId(request.getOperatorId());

		ArrayOfBasketItems arraybasketItems = new ArrayOfBasketItems();

		List<BasketItem> basketItemlist = new ArrayList<>();
		if (request.getItems() != null) {
			List<Items> items = request.getItems();

			for (Items item : items) {
				BasketItem basketItem = new BasketItem();

				if (item.getSku() != null) {
					basketItem.setSku(item.getSku());
				}
				if (item.getPrice() != null) {
					basketItem.setPrice(item.getPrice());
				}

				ArrayOfBasketItemAttributes arrayofbasketitemattributes = new ArrayOfBasketItemAttributes();

				List<BasketItemAttribute> basketitemattributelist = new ArrayList<>();
				if (item.getAttributes() != null) {
					extractedAttributs(item, basketitemattributelist);
				}
				if (item.getEncryptedAttribute() != null) {
					basketItem.setEncryptedAttributes(item.getEncryptedAttribute().getBytes());
				}
				arrayofbasketitemattributes.getAttribute().addAll(basketitemattributelist);
				basketItem.setAttributes(arrayofbasketitemattributes);
				basketItemlist.add(basketItem);
			}
		}
		arraybasketItems.getBasketItem().addAll(basketItemlist);
		basket.setItems(arraybasketItems);
		message.setBasket(basket);
		btselrequest.setMessage(message);

		return btselrequest;

	}

	private void extractedAttributs(Items item, List<BasketItemAttribute> basketitemattributelist) {
		List<Attributes> attributes = item.getAttributes();

		for (Attributes attribute : attributes) {
			BasketItemAttribute basketitemattribute = new BasketItemAttribute();

			if (attribute.getName() != null) {
				basketitemattribute.setName(attribute.getName());
			}

			if (attribute.getValue() != null) {
				basketitemattribute.setValue(attribute.getValue());
			}

			basketitemattributelist.add(basketitemattribute);

		}
	}

	public EposBasketResponse addItemsResponse(ResponseBean<AddToEposBasketResponse> btselResponse) {

		EposBasketResponse response = new EposBasketResponse();

		if (btselResponse != null && btselResponse.getData() != null && btselResponse.getData().getMessage() != null)

		{
			AddToEposBasketResponse.Message message = btselResponse.getData().getMessage();

			response.setBasketId(message.getBasketId());
			response.setBasketStatus(message.getBasketStatus().value());
		}

		return response;
	}

}
